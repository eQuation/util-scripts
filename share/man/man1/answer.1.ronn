answer(1) -- user input collector
=============================================

# MIT License
# .
# Copyright (c) 2018 John Mollberg
# .
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# .
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# .
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## SYNOPSIS

`answer`

## DESCRIPTION

**answer** is a simple tester that will collect input from a user.

## EXAMPLES

1. Collect input from a good user:

    ```
    $ answer && echo yep || echo nope
    enter (y/n): y
    yep
    ```

1. Collect input from a bad user:

    ```
    $ answer && echo yep || echo nope
    enter (y/n): a
    try again: b
    try again: c
    try again: n
    nope
    ```

## AUTHORS

John Mollberg

## SEE ALSO

test(1)

