# Util Scripts

Collection of utility scripts, mostly in `bash` and `python`.

## Getting Started

### Prerequisites

The following must be installed:

* [bash](https://www.gnu.org/software/bash/manual/)
* [git](https://git-scm.com/doc)

### Installing

#### Quick and Easy

The scripts may all be automatically installed by running

```
bash -c "$( curl -fsSL https://gitlab.com/eQuation/util-scripts/raw/master/tools/install )"
```

This will clone the repository under `~/.util-scripts` and instruct the user on
how to alter their path.

#### Options

* if the project should be installed with
[symlinks](https://en.wikipedia.org/wiki/Symbolic_link) created somewhere
such as `~/bin` _to_ the util scripts, it can be done by providing the path as
the first argument like this:

```
bash -c "$( curl -fsSL https://gitlab.com/eQuation/util-scripts/raw/master/tools/install ) ~/bin"
```

This will also create symlinks to the man pages in `~/share/man`.

* If the repository should be cloned somewhere other than `~/.util-scripts` such
as `~/tools`, it can be done with the second parameter like this:

```
bash -c "$( curl -fsSL https://gitlab.com/eQuation/util-scripts/raw/master/tools/install ) '' ~/tools"
```

Notice how the first argument (the symlink path) is an empty string. This tells
the installer to install the repository at ~/tools, but not to create a symlink
path. The first argument could just as easily be set to `~/bin` or any other
directory.

## Upgrading

Over time, more scripts may be added to this project and bugs may be fixed.
To keep the installation up to date, use the `utils` command:

```
utils upgrade
```

Note: using `utils` *requires* that these scripts (or symlinks, if it was installed that
way) are on the `$PATH`, since upgrading takes advantage of the scripts this
project contains.

## Versioning

This project uses [SemVer](http://semver.org/) for versioning. For the versions
available, see the
[tags on this repository](https://gitlab.com/eQuation/util-scripts/tags).

## Author

* **John Mollberg** - *Sole author*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
